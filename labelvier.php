<?php
/*
Plugin Name: Label Vier Extras
Plugin URI: https://labelvier.nl
Description: Optimisations for the current wordpress theme.
Author: Label Vier
Version: 0.10.3
*/

/**
 * define plugin version
 */
define( 'LABELVIER_PLUGIN_VERSION', '0.10.3' );

//action schedular needs to be loaded before plugins_loaded => 0.
require_once( plugin_dir_path( __FILE__ ) . '/modules/action-scheduler/action-scheduler.php' );

/**
 * init all functions for the labelvier plugin, all functions can be suppressed with a filter
 */
function init_labelvier_plugin() {
	/**
	 * Enqueue scripts and styles.
	 */
	function labelvier_kit_scripts() {
		wp_enqueue_script( 'labelvier-starter-js', plugin_dir_url( __FILE__ ) . '/labelvier.js', array(), LABELVIER_PLUGIN_VERSION, true );
	}
	add_action( 'wp_enqueue_scripts', 'labelvier_kit_scripts' );

	/**
	 * Enable lazy loading
	 */
	if ( apply_filters( 'labelvier/use_lazy_loading', false ) ) {
		require( __DIR__ . '/modules/lazy-loading/create-lazy-image.php' );
	}

	/**
	 * Enable shortpixel compression
	 */
	if ( apply_filters( 'labelvier/use_shortpixel', true ) ) {
		require( __DIR__ . '/modules/shortpixel/shortpixel.php' );
	}

	/**
	 * Enable focal point functionality for thumbnails
	 */
	if ( apply_filters( 'labelvier/use_focal_point', false ) ) {
		require( __DIR__ . '/modules/focal-point/focal-point.php' );
	}

	/**
	 * Enable updates from bitbucket master branch
	 */
	require( __DIR__ . '/modules/update-checker/init.php' );
}
add_action( 'after_setup_theme', 'init_labelvier_plugin', 10, 0 );
