<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 20-01-2020
 * Time: 16:11
 */

/*
 * Create a queue to compress images with a cron action (prevents long loading times)
 */
add_filter( 'wp_generate_attachment_metadata', function ( $metadata, $attachment_id, $context ) {
	if ( ! isset( $metadata['file'] ) ) {
		return $metadata;
	}

	//schedule job
	if ( false === as_next_scheduled_action( 'labelvier_compress_images', [ 'attachment_id' => $attachment_id ] ) ) {
		as_enqueue_async_action( 'labelvier_compress_images', [ 'attachment_id' => $attachment_id ]);
	}

	//return filter
	return $metadata;
}, 10, 3 );

/**
 * Compress the image (runs from action scheduler)
 *
 * @param $metadata
 *
 * @return bool
 */
add_action( 'labelvier_compress_images', static function ( $attachment_id ) {
	try {
		// Load attachment meta data
		$metadata = wp_get_attachment_metadata( $attachment_id );
		// Set up the API Key.
		$api_key     = get_field( 'shortpixel_api_key', 'option' );
		$compression = (int) get_field( 'shortpixel_level', 'option' );
		if ( ! $api_key || empty( $compression ) || $compression == - 1 ) {
			return true;
		}
		require_once( __DIR__ . '/vendor/autoload.php' );
		ShortPixel\setKey( $api_key );

		// Get upload dir
		$upload_dir  = wp_upload_dir();
		$base_dir    = trailingslashit( $upload_dir['basedir'] );
		$upload_path = trailingslashit( $upload_dir['path'] );

		// Compress with a specific compression level: 0 - lossless, 1 - lossy (default), 2 - glossy
		// Resize original
		echo $original_file_path = $base_dir . $metadata['file'];
		ShortPixel\fromFile( $original_file_path )->optimize( $compression )->toFiles( $upload_path );

		// resize thumbs, using the basedir from the original file
		$thumb_sizes = get_field( 'compress_thumbnails', 'option' );
		foreach ( $metadata['sizes'] as $key => $size ) {
			if ( in_array( $key, $thumb_sizes, false ) ) {
				$thumb_file = $upload_path . $size['file'];
				ShortPixel\fromFile( $thumb_file )->optimize( $compression )->toFiles( $upload_dir['path'] );
			}
		}
		return true;
	} catch ( Exception $e ) {
		return false;
	}
}, 10, 1);


add_filter( 'acf/load_field/name=compress_thumbnails', static function ( $field ) {
	global $_wp_additional_image_sizes;
	$default_image_sizes = get_intermediate_image_sizes();
	$image_sizes         = [];
	foreach ( $default_image_sizes as $size ) {
		$image_sizes[ $size ]['width']  = (int) get_option( "{$size}_size_w" );
		$image_sizes[ $size ]['height'] = (int) get_option( "{$size}_size_h" );
		$image_sizes[ $size ]['crop']   = get_option( "{$size}_crop" ) ?: false;
	}

	if ( isset( $_wp_additional_image_sizes ) && count( $_wp_additional_image_sizes ) ) {
		$image_sizes = array_merge( $image_sizes, $_wp_additional_image_sizes );
	}
	foreach ( $image_sizes as $name => $sizes ) {
		$field['choices'][ $name ] = $name . ' (' . $sizes['width'] . 'x' . $sizes['height'] . ')';
	}

	return $field;
}, 10, 1 );

add_filter( 'labelvier_theme_settings_fields', function ( $fields ) {
	$shortpixel_fields = [
        array(
            'key'               => 'field_5e32a9aead79d',
            'label'             => 'ShortPixel',
            'name'              => '',
            'type'              => 'tab',
            'instructions'      => '',
            'required'          => 0,
            'conditional_logic' => 0,
            'wrapper'           => array(
                'width' => '',
                'class' => '',
                'id'    => '',
            ),
            'placement'         => 'top',
            'endpoint'          => 0,
        ),
		array(
			'key'               => 'field_5e26adfefbe10',
			'label'             => 'Afbeeldingen optimaliseren',
			'name'              => 'shortpixel_level',
			'type'              => 'radio',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'           => array(
				'-1' => 'Afbeeldingen niet optimaliseren',
				'1'  => 'Maximale compressie',
				'2'  => 'Normale compressie',
				'0'  => 'Minimale compressie',
			),
			'allow_null'        => 0,
			'other_choice'      => 0,
			'default_value'     => - 1,
			'layout'            => 'vertical',
			'return_format'     => 'value',
			'save_other_choice' => 0,
		),
		array(
			'key'               => 'field_5e26ae79fbe11',
			'label'             => 'Shortpixel API key',
			'name'              => 'shortpixel_api_key',
			'type'              => 'password',
			'instructions'      => '',
			'required'          => 1,
			'conditional_logic' => array(
				array(
					array(
						'field'    => 'field_5e26adfefbe10',
						'operator' => '!=empty',
					),
					array(
						'field'    => 'field_5e26adfefbe10',
						'operator' => '!=',
						'value'    => '-1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'append'            => '',
			'maxlength'         => '',
		),
		array(
			'key'               => 'field_5e26ae9afbe13',
			'label'             => 'Afbeeldingen die geoptimaliseerd moeten worden',
			'name'              => 'compress_thumbnails',
			'type'              => 'checkbox',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => 'field_5e26adfefbe10',
						'operator' => '!=empty',
					),
					array(
						'field'    => 'field_5e26adfefbe10',
						'operator' => '!=',
						'value'    => '-1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'choices'           => array(
				'thumbnail'      => 'thumbnail (150x150)',
				'medium'         => 'medium (300x300)',
				'medium_large'   => 'medium_large (768x0)',
				'large'          => 'large (1024x1024)',
				'1536x1536'      => '1536x1536 (1536x1536)',
				'2048x2048'      => '2048x2048 (2048x2048)',
				'header-desktop' => 'header-desktop (1280x700)',
				'header-tablet'  => 'header-tablet (1024x560)',
				'header-mobile'  => 'header-mobile (700x383)',
			),
			'allow_custom'      => 0,
			'default_value'     => array(),
			'layout'            => 'vertical',
			'toggle'            => 0,
			'return_format'     => 'value',
			'save_custom'       => 0,
		)
	];
	$fields            = array_merge( $fields, $shortpixel_fields );

	return $fields;
}, 10, 1 );
