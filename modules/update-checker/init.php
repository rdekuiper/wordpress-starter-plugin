<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 24-01-2020
 * Time: 13:59
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'http://updater.labelvier.nl/wp-starter-kit/plugin.json',
	WP_PLUGIN_DIR . '/labelvier/labelvier.php',
	'labelvier',
	12,
	'labelvier_update_checker'
);
