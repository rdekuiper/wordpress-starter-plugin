/**
 * Import the module scripts
 */
import './polyfills/foreach';
import './polyfills/intersection-observer';
import './lazy-loading/lazy-loading';
