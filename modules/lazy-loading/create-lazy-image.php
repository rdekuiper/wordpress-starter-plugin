<?php
/**
 * Lazy image function file
 *
 * @package ibpix
 */

/**
 * Creates a images with enables lazy loading
 *
 * @package ibpix
 * @author Sebas & Eric
 *
 * @param string $id the icon id.
 * @param string $size the icon size.
 * @param string $sizes example: "(max-width: 640px) 100vw, 640px".
 * @return mixed html output.
 */
add_filter('wp_get_attachment_image_attributes', 'labelvier_create_lazy_image', 10, 3);
function labelvier_create_lazy_image($attr, $attachment, $size) {
	if(is_admin()) {
		return $attr;
	}
	$id = $attachment->ID;

	//create svg placeholder
	$image   = wp_get_attachment_image_src( $id, $size );
	$srcset  = wp_get_attachment_image_srcset( $id, $size );
	$src_svg = "data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg' viewBox%3D'0 0 " . $image[1] . '  ' . $image[2] . "'%2F%3E";

	$attr['src'] = $src_svg;
	$attr['data-src'] = $image[0];
	$attr['data-srcset'] = $srcset;
	$attr['class'] .= " js-lazy anim-fadein";
	$attr['width'] = $image[1];
	$attr['height'] = $image[2];
	unset($attr['src-set']);

	return $attr;

}

// debugging for gutenberg
//add_filter('wp_get_attachment_metadata', function($data, $post_id) {
//	unset($data['sizes']);
//	return $data;
//}, 999, 2);
