import {dest, parallel, series, src} from 'gulp';
import args from 'yargs';
import del from 'del';
import webpack from 'webpack-stream';
import zipr from 'gulp-zip';
import bumper from 'gulp-bump';

const PRODUCTION = args.argv.prod;

export const scripts = () => {
    return src('./modules/main.js')
        .pipe(webpack({
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: []
                            }
                        }
                    }
                ]
            },
            mode: PRODUCTION ? 'production' : 'development',
            devtool: !PRODUCTION ? 'inline-source-map' : false,
            output: {
                filename: './labelvier.js'
            },
        }))
        .pipe(dest('./'));
};
export const cleanDist = () => del(['./labelvier.js']);

export const cleanBuild = () => del(['./../../../build/labelvier'], {force: true});
export const copyToBuild = () => {
    src([
        "./**/*",
        "!node_modules{,/**}",
        "!./**/*.js",
        "!./**/*.json",
        "./labelvier.js"
    ])
        .pipe(dest('./../../../build/labelvier'));

    return src('plugin.json')
        .pipe(dest('./../../../build'))
};

export const bump = done => {
    let kind = 'patch';
    if (args.argv.major) {
        kind = 'major';
    }
    if (args.argv.minor) {
        kind = 'minor';
    }
    if (args.argv.patch) {
        kind = 'patch';
    }

    const bumpVersion = {
        type: kind
    };

    var constant = "LABELVIER_PLUGIN_VERSION";
    const constantVersion = {
        key: 'constant', // for error reference
        type: kind,
        regex: new RegExp('([<|\'|"]?(' + constant + ')[>|\'|"]?[ ]*[:=,]?[ ]*[\'|"]?[a-z]?)(\\d+.\\d+.\\d+)(-[0-9A-Za-z.-]+)?(\\+[0-9A-Za-z\\.-]+)?([\'|"|<]?)', 'i')
    }

    if(args.argv.to) {
        delete bumpVersion.type;
        bumpVersion.version = args.argv.to;

        delete constantVersion.type;
        constantVersion.version = args.argv.to;
    }

    src('./labelvier.php')
        .pipe(bumper(bumpVersion))
        .pipe(bumper(constantVersion))
        .pipe(dest('./'));

    src('./plugin.json')
        .pipe(bumper(bumpVersion))
        .pipe(dest('./'));

    done();
};

// Define tasks
export const dev = series(cleanDist, parallel(scripts));
export const build = series(cleanBuild, parallel(scripts), copyToBuild);
export default dev;
